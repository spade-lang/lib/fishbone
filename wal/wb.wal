(defmacro rise [s]
  `(&& (= (reval ,s 1) 0) ,s))

(defmacro fall [s]
  `(&& (reval ,s 1) (= ,s 0)))

(defun safe-round-average [xs]
  (if xs
      (round (average xs))
      "-"))

(defun track-wb [g]
  (in-spade-struct fishbone::wishbone::Wishbone g
    (define reads '())
    (define writes '())
    (define errors 0)
    (define potential-cycles (count (&& (rise #clk) (= #rst 0))))
    (define used_cycles (count (&& (&& (rise #clk) (= #rst 0)) (&& #cyc #stb))))
    (whenever (&& (rise #clk) (= #rst 0))
              (when (&& #cyc #stb)
                (let ([start-at TS]
                      [nclocks 0])
                  (while (&& (! (|| (= 1 #ack) (= 1 #err)))
                             (!= INDEX MAX-INDEX))
                    (when (rise #clk)
                      (inc-safe nclocks))
                    (step))
                  ;; check if inside an transaction and EOW is reached
                  (unless (&& (= INDEX MAX-INDEX) (! (|| (= 1 #ack) (= 1 #err))))
                    (when #err (inc-safe errors))
                    (if (&& 1 (= #write[16] 0))
                        (set [writes (append writes nclocks)])
                        (set [reads (append reads nclocks)])))))))

  (array ['reads reads]
         ['reads-avg-ack (safe-round-average reads)]
         ['writes writes]
         ['writes-avg-ack (safe-round-average writes)]
         ['inactive-cycles (round (* (- 1 (/ used_cycles potential-cycles)) 100))]
         ['errors errors]))


(defun start-pass []
  (get-histogram/ack)
  (define all-wb-busses (spade-struct fishbone::wishbone::Wishbone [clk rst stb cyc err write]))
  (log/info "Analyzing Wishbone interfaces ")
  (for [bus all-wb-busses]
       (let ([results (timeframe (track-wb bus))])
         (log/analysis "# " bus)
         (log/analysis "   Nr. transactions : " (+ (length (geta results 'reads)) (length (geta results 'writes))))
         (log/analysis "   Nr. reads        : " (length (geta results 'reads)))
         (log/analysis "   Avg read latency : " (geta results 'reads-avg-ack) " clock cycles")
         (log/analysis "   Nr. writes       : " (length (geta results 'writes)))
         (log/analysis "   Avg write latency: " (geta results 'writes-avg-ack) " clock cycles")
         (log/analysis "   Errors           : " (geta results 'errors))
         (log/analysis "   Inactive cycles  : " (geta results 'inactive-cycles) "%"))))
        

(defun get-histogram/ack []
  (define all-wb-busses (spade-struct fishbone::wishbone::Wishbone [clk rst stb cyc err write]))
  (generate-histogram
   (for/list [g all-wb-busses]
             (in-spade-struct fishbone::wishbone::Wishbone g
                      (map (fn [t] TS@t) (find (&& #clk
                                 (= #rst 0)
                                    #stb
                                    #cyc
                                   (|| (= 1 #ack) (= 1 #err)))))))
   all-wb-busses))
