import subprocess

import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from pathlib import Path

from wal.spade import WalAnalysisPass

class WbHistogrammPass(WalAnalysisPass):

    def __init__(self, pass_dir, wavefile):
        wavefile = Path(wavefile)
        wavefile_fst = wavefile.with_suffix('.fst')
        subprocess.run(['vcd2fst', wavefile, wavefile_fst])
        super().__init__(pass_dir, wavefile_fst)
        self.wal.register_operator('generate-histogram',
                                   lambda seval, args: self.generate_histogram(seval, args))

    def generate_histogram(self, seval, args):
        data = seval.eval(args[0])
        labels = [l[len(self.top)+1:] for l in seval.eval(args[1])]
        data_len = list(enumerate(map(len, data)))
        max_data = sorted(data_len, key=lambda x: x[1])[-1]

        hist_data = np.array(max_data)
        q25, q75 = np.percentile(hist_data,[.25,.75])
        bin_width = 2*(q75 - q25)*len(hist_data)**(-1/3)
        num_bins = int(((hist_data.max() - hist_data.min())/bin_width) * 0.3)
        num_bins = int(max(map(len, data))/10)
        plt.tick_params(
            axis='both',
            which='both',
            bottom=False,
            top=False,
            left=False,
            right=False,
            labelbottom=False)
        n, bins, patches = plt.hist(data, num_bins, histtype='barstacked', label=labels)
        plt.yticks([])
        plt.xlabel('Simulation Time')
        plt.ylabel('Interface Activity')
        
        plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.04),
                   fancybox=True, shadow=True, ncol=5)
        file = self.wavefile.parent.joinpath('wb_histogram.png')
        plt.savefig(file, dpi=600)
        print("Wrote wishbone histogram to {file}")

    def run(self):
        self.wal.eval_str('(require wb)')
        self.wal.eval_str('(start-pass)')
